'use strict';

module.exports = function($rootScope, $timeout) {
  'ngInject';

  var alerts = [];

  return {
    alerts: alerts,

    add: function(alert) {
      var self = this;
      this.alerts.push({
        type: alert.type,
        msg: alert.msg,
        close: function() {
          self.remove(this);
        }
      });

      $timeout(function() {
        //self.remove(this)
      },5000);
    },

    remove: function(alert) {
      this.removeAt(this.alerts.indexOf(alert));
      $rootScope.$broadcast('ALERT_REMOVED', alert);
    },

    removeAt: function(index) {
      this.alerts.splice(index, 1);
    }
  };
};
