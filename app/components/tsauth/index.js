'use strict';

var angular = require('angular');

/**
 * Directive for authenticating with Apigee SSO
 */
module.exports = angular.module('app.components.tsauth', [
  require('tsavatar').name
])
  .directive('tsAuth', function() {
    return {
      restrict: 'E',
      template: require('./template.html'),
      controller: 'tsAuthCtrl'
    };
  })
  .controller('tsAuthCtrl', require('./controller'))
  .service('tsAuthService', require('./service'));
