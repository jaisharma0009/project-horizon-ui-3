'use strict';

var URI = require('uri.js')

//TODO: generate state and nonce
//'https://accounts.apigee.com/accounts/sign_in?response_type=token&client_id=apiplatform',
//'https://login.apigee.com/oauth/authorize?client_id=360-external-webapp&response_type=token&redirect_uri=http://alpha360.apigee.com:3000/oAuthImplicitCallback.html&state=d89c38f69d5dc98240a23754e4ebdf5c&nonce=d89c38f69d5dc98240a23754e4ebdf5c',
var MYAPIGEE_API_KEY = 'mrtZwVh2scOL4cL0qJjxIeCbpos9mC4v',
  SIGN_IN_URL,
  SIGN_OUT_URL = 'https://enterprise.apigee.com/logout';

if (document.location.hostname === 'alpha360.apigee.com') {
  SIGN_IN_URL = 'https://login.apigee.com/oauth/authorize?client_id=alpha360-external-webapp&response_type=token';
  SIGN_IN_URL.concat('&redirect_uri=https://alpha360.apigee.com/oAuthImplicitCallback.html')
    .concat('&state=d89c38f69d5dc98240a23754e4ebdf5c')
    .concat('&nonce=d89c38f69d5dc98240a23754e4ebdf5c');
} else if (document.location.hostname === '360.apigee.com') {
  SIGN_IN_URL = 'https://login.apigee.com/oauth/authorize?client_id=360-external-webapp&response_type=token';
  SIGN_IN_URL.concat('&redirect_uri=https://360.apigee.com/oAuthImplicitCallback.html')
    .concat('&state=d89c38f69d5dc98240a23754e4ebdf5c')
    .concat('&nonce=d89c38f69d5dc98240a23754e4ebdf5c');
} else if (document.location.hostname === 'cslandandexpand.googleplex.com') {
  SIGN_IN_URL = 'https://login.apigee.com/oauth/authorize?client_id=360-external-webapp&response_type=token';
  SIGN_IN_URL.concat('&redirect_uri=https://cslandandexpand.googleplex.com/oAuthImplicitCallback.html')
    .concat('&state=d89c38f69d5dc98240a23754e4ebdf5c')
    .concat('&nonce=d89c38f69d5dc98240a23754e4ebdf5c');
}

/**
 * @ngInject
 */
module.exports = function tsAuthService($location, $state, $window, $http,
  $rootScope, Restangular) {

  var self = this;
  this.lastState;

  function loadUser() {

    try {
      var userInfo = window.localStorage.getItem('user');

      if (!userInfo)
        return {};

      userInfo = JSON.parse(userInfo);
      
      return userInfo;

    } catch (ex) {
      return {};
    }
  }

  function urlWithCallback(url) {
    var callbackUrlObj = URI($location.absUrl());
    callbackUrlObj.fragment('');
    callbackUrlObj.path('/oAuthImplicitCallback.html');

    var urlObj = URI(url);
    urlObj.addQuery({
      redirect_uri: callbackUrlObj.toString()
    });
    return urlObj.toString();
  }

  this.user = loadUser();

  this.fullRequestInterceptor = function (element, operation, what, url,
    headers, params) {

    var auth = window.localStorage.getItem('Authorization');

    delete headers.Authorization;

    if (this.user && auth) {

      headers.Authorization = auth;

    }

    params.apikey = MYAPIGEE_API_KEY;

    return {
      headers: headers,
      params: params
    };
  }.bind(this);

  this.signedIn = function () {
    if(ENV == 'local') {
      return true;
    }

    //check if user has SSO token
    var auth = window.localStorage.getItem('Authorization');
    if (!auth) {
      return false;
    }
    return true;

  };


  var port = '';
  if ($location.$$port != 80 && $location.$$port != 443) {
    port = ':' + $location.$$port;
  }
  var redirectUri = $location.$$protocol + '://' + $location.$$host + port + '/oAuthImplicitCallback.html';
  var clientId = '1039371895347-hjjhcddiff0od7fimsmu8rrf7c9njhsq.apps.googleusercontent.com';
  var scope = 'https://www.googleapis.com/auth/userinfo.profile';
      scope = encodeURIComponent('https://www.googleapis.com/auth/userinfo.email')+'+'+encodeURIComponent('https://www.googleapis.com/auth/userinfo.profile');
  var passthroughState = '';


  var loginUrl = 'https://accounts.google.com/o/oauth2/v2/auth?';
  loginUrl += 'scope=' + scope;
  loginUrl += '&access_type=offline';
  loginUrl += '&include_granted_scopes=true';
  loginUrl += '&state=' + encodeURIComponent(passthroughState);
  loginUrl += '&redirect_uri=' + encodeURIComponent(redirectUri);
  loginUrl += '&response_type=code';
  loginUrl += '&client_id=' + encodeURIComponent(clientId);

  this.loginUrl = loginUrl;

  // Copy the bearer token to the clipboard for use with Postman...
  this.copyToken = function () {

    var toCopy = $window.localStorage.getItem('Authorization');

    var body = angular.element($window.document.body);
    var textarea = angular.element('<textarea/>');
    textarea.css({
      position: 'fixed',
      opacity: '0'
    });

    textarea.val(toCopy);
    body.append(textarea);
    textarea[0].select();

    try {
      var successful = document.execCommand('copy');
      if (!successful) throw successful;
    } catch (err) {
      window.prompt("Copy to clipboard: Ctrl+C, Enter", toCopy);
    }

    textarea.remove();

  };

  this.redirect = function () {
    if (this.signedIn()) {
      var state = window.localStorage.getItem('lastState') || 'customers';
      //$state.go(state);
    }else{
      $state.go('login');
    }
  }

  this.signIn = function () {
    this.user.redirectPath = $location.path();

    $rootScope.$evalAsync(function () {
      $window.location.replace(urlWithCallback(SIGN_IN_URL));
    });

  };

  this.signOut = function () {
    this.clearCredentials();
    $state.go('login');
  };

  this.clearCredentials = function () {
    this.user = {};
    window.localStorage.removeItem('Authorization');
    window.localStorage.removeItem('user');
  };

  this.isApigeeUser = function () {
    if (this.user.email && (this.user.email.indexOf('@apigee.com') > 0 || this.user.email.indexOf('@google.com') > 0)) {
      return true
    }
    return false;
  }

};