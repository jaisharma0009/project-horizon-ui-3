module.exports = `<div class="collapsePnl" id="${uuid}">
                            <span id="tsc-columns" style="display:none">
                              Columns
                              <i class="fa fa-caret-up" aria-hidden="true"></i>
                            </span>
                            <span id="tsc-filters" style="display:none">
                              Filters
                              <i class="fa fa-caret-up" aria-hidden="true"></i>
                            </span>
                            <span id="tsc-export" style="display:none">
                              Export
                              <i class="fa fa-caret-up" aria-hidden="true"></i>
                            </span>
                            <div class="collapseBtn">
                              <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                            </div>
                          </div>`;