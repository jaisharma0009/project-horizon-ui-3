'use strict';

var angular = require('angular');

/**
 * Directive for displaying a person's name, avatar, and contact info
 */
module.exports = angular.module('app.components.tscontact', [
  require('tsavatar').name
])
  .directive('tsContact', function() {
    return {
      restrict: 'E',
      scope: {
        name: '@',
        email: '@',
        title: '@'
      },
      template: require('./template.html')
    };
  });
