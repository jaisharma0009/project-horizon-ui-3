'use strict';

var angular = require('angular');
var $ = require('jquery');
var _ = require('lodash');
var scrollbar = require("jquery.scrollbar");

module.exports = angular.module('app.components.tsmaterializegrid', [])
  .directive('tsMaterializeGrid', function ($rootScope, $timeout) {
    return {
      restrict: 'A',
      require: 'uiGrid',
      scope: false,
      link: function (scope, element, attrs, gridScope) {
        var currentMousePos = { x: 0, y: 0 };
        element.addClass('material-grid');

        $(document).ready(function () {

          $('.ui-grid-viewport').scrollbar();

          $(document).mousemove(function (event) {
            currentMousePos.x = event.pageX;
            currentMousePos.y = event.pageY;
            $rootScope.$broadcast('MOUSE_MOVE', currentMousePos);
          });

          $('.ui-grid-filter-input').on('click', function () {
            var parent = $(this).parent().parent().parent().parent();
            var label = parent.find('.ui-grid-header-cell-label');
            label.addClass('materialized');
            $(this).addClass('focused');
          });

          $('.ui-grid-filter-input').on('focus', function () {
            var parent = $(this).parent().parent().parent().parent();
            var label = parent.find('.ui-grid-header-cell-label');
            label.addClass('materialized');
            $(this).addClass('focused');
          });

          $timeout(() => {
            _.each($('.ui-grid-filter-input'), e => {
              e.focus();
              e.blur();
            });
          }, 600);

          scope.$on('TSS_CLICK', () => {
            _.each($('.ui-grid-filter-input'), e => {
              e.focus();
              e.blur();
            });
          });

          $('.ui-grid-filter-input').on('blur', function () {
            if (!$(this).val()) {
              var parent = $(this).parent().parent().parent().parent();
              var label = parent.find('.ui-grid-header-cell-label');
              label.removeClass('materialized');
              $(this).removeClass('focused');
            }
          });

          $('.ui-grid-icon-container').on('click', function () {
            var height = $('.ui-grid-viewport').height();
            $(this).parent().find('.ui-grid-menu-mid').height(height - 12);
          });

          $('.ui-grid-icon-container').click(function () {
            var menu = $(this).parent().find('.ui-grid-menu-mid');
            $timeout(() => {
              if (menu.is(":visible")) {
                $(this).parent().addClass('pressed');
              } else {
                $(this).parent().removeClass('pressed');
              }
            }, 50);
          });


          $('body').append('<div id="grid-popover"></div>');
          let alertA = function () {
            let popover = $('#grid-popover');
            let x =  currentMousePos.x;

            let offset = $('body').width() - currentMousePos.x;
            if(offset < 200) { x -= 150; }

            let position = {
              left: x + 'px',
              top: (currentMousePos.y - 30) + 'px'
            }
            
            popover.html(this.title);
            popover.show();
            popover.css(position);
          }

          var addTooltips = function () {
            _.each($('.ui-grid-cell-contents'), obj => {
              let parent = $(obj).parent().parent();
              let isHeader = parent.hasClass('ui-grid-header-cell');

              let container = $(obj).parent().parent();
              if (!isHeader) {
                container = $(obj);
              }

              let title = $(obj).attr('title');
              if (title) {
                let pos = container.offset();
                let vals = { title: title, position: pos };

                container.on('mouseenter', alertA.bind(vals));
                container.on('mouseleave', () => {
                  $('#grid-popover').hide();
                });
                $(obj).attr('title', '');
              }
            });
          }
          addTooltips();

          $(window).on('resize', () => {
            addTooltips();
          });

          $('body').on('click', function () {
            $('.ui-grid-menu-button').removeClass('pressed');
          });
        });
      }
    };

  });
