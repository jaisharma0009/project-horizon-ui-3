'use strict';

var angular = require('angular');

/**
 * Directive for displaying a support case priority
 */
module.exports = angular.module('app.components.tspriority', [])
  .directive('tsPriority', function() {
    return {
      restrict: 'E',
      scope: {
        priority: '='
      },
      template: require('./template.html'),
      controller: 'tsPriorityCtrl'
    };
  })
  .controller('tsPriorityCtrl', require('./controller'));
