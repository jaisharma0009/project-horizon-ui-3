'use strict';

module.exports = class TsswitchController {
  constructor($scope, $rootScope) {
    'ngInject';

    this.$rootScope = $rootScope;
    this.switch = false;

    $scope.$watch(() => this.initialState, newVal => {
      this.switch = newVal;
    })
  }

  toggle() {    
    this.switch ? this.toggleOff() : this.toggleOn();

    let id = !!this.id ? `-${this.id}` : '';
    this.$rootScope.$broadcast(`tsSwitchToggle${id}`);
  }

  toggleOn() {
    this.switch = true;
    this.$rootScope.$broadcast('tsSwitchOn');

    if(this.ngOn) {
      this.ngOn();
    }
  }

  toggleOff() {
    this.switch = false;
    this.$rootScope.$broadcast('tsSwitchOff');

    if(this.ngOff) {
      this.ngOff();
    }
  }

}