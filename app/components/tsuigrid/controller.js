'use strict';

/**
 * @ngInject
 */
module.exports = function($scope, $rootScope) {
  
  // debugger;
  $scope.options = {};
  $scope.title = 'UI Grid';
  
  // $scope.options = options;

  $scope.$watch('options', function(options) {
    debugger;
    console.log('options updated', options);
    $scope.options = options;
  }, true);

  $scope.$watch('title', function(title) {
    $scope.title = title;
  });

};
