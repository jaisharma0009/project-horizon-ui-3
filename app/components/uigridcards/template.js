var _ = require('lodash');

var classes = 'col-xs-12 col-sm-6 col-md-4 col-lg-3';
var fullCol = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
var dual = 'col-xs-12 col-sm-6 col-md-6 col-lg-6 dual-col';

var thirds = 'col-xs-12 col-sm-4 col-md-4 col-lg-4';
var thirdsc = 'col-xs-12 col-sm-8 col-md-8 col-lg-8';

var thirds2 = 'col-xs-6 col-sm-4 col-md-4 col-lg-4';
var thirds2c = 'col-xs-6 col-sm-8 col-md-8 col-lg-8';

var thirds3 = 'col-xs-12 col-sm-6 col-md-4 col-lg-4';

var fourths2 = 'col-xxs-12 col-xs-6 col-sm-4 col-md-4 col-lg-3 col-xlg-2';

var others = 'col-xs-12 col-sm-5 col-md-5 col-lg-5';
var othersc = 'col-xs-12 col-sm-7 col-md-7 col-lg-7';

var sizeOptions = {fullCol, dual, thirds, thirdsc, others, othersc};

var defaultTemplate = (columnDefs, classes = 'thirds') => {
  let size = sizeOptions[classes];
  let template = '';

  columnDefs = _.orderBy(columnDefs, ['cardIndex']);
  _.each(columnDefs, item => {
    let cardClass = !!item.cardClass ? item.cardClass : '';

    template += `<div class="${size} ${cardClass}" title="{{item.entity.${item.field}}}" ng-show="columnVisibility['${item.field}']">
                   <label>${displayName(columnDefs, item.field)}</label>
                   ${parseTemplate(columnDefs, item.field)}
                 </div>`;
  });

  return template;
}

var listTemplate = columnDefs => {
  let template = '';
  columnDefs = _.orderBy(columnDefs, ['cardIndex']);

  template += `
    <div class="col-xs-3 hidden-md hidden-lg"></div>
    <div class="double-card ${thirds2} logo-container">
      <div class="card-logo">
        <img src="{{item.entity.account_logo}}">
      </div>
    </div>
    <div class="double-card ${thirdsc}">
      <div class="account_name" title="{{item.entity.account_name}}" ng-show="columnVisibility['account_name']">
        <h4>${parseTemplate(columnDefs, 'account_name')}</h4>
      </div>
      <div title="Last 90 days Traffic" ng-show="columnVisibility['90d_traffic']">
        ${parseTemplate(columnDefs, '90d_traffic')}
      </div>
      <div title="{{item.entity.sales_rep_email}}" ng-show="columnVisibility['sales_rep_email']">
        <label>${displayName(columnDefs, 'sales_rep_email')}</label>
        ${parseTemplate(columnDefs, 'sales_rep_email')}
      </div>
    </div>`;

  let cols = ['account_name', '90d_traffic', 'sales_rep_email'];
  _.each(cols, i => {
    let index = _.findIndex(columnDefs, {field: i});
    columnDefs.splice(index, 1);
  })

  _.each(columnDefs, item => {
      template += `<div class="${thirds2}" title="{{item.entity.${item.field}}}" ng-show="columnVisibility['${item.field}']">
                    <label>${displayName(columnDefs, item.field)}</label>
                    ${parseTemplate(columnDefs, item.field)}
                  </div>`;
  });

  return template;
}


var engageTemplate = columnDefs => {
  let template = '';
  columnDefs = _.orderBy(columnDefs, ['cardIndex']);

  template = `<div class="engage-card">
                <div class="green-title">
                  <div class="${dual}">${parseTemplate(columnDefs, 'account_name')}</div>
                  <div class="${dual} opp_name">${parseTemplate(columnDefs, 'opportunity_name')}</div>
                </div>
                <div class="${dual} tall-row">
                  <label>Stage:</label>
                  <div class="sales-stage">
                    <ts-percentage type="steps" value="3" total="7"></ts-percentage>
                    <span>{{item.entity.sales_stage}}</span>
                  </div>
                </div>
                <div class="${dual} amount tall-row">
                  <label>Amount:</label>
                  ${parseTemplate(columnDefs, 'current_opportunity_amount')}
                </div>
                <div class="${dual}">
                  <label>Marketing engagement:</label>
                  ${parseTemplate(columnDefs, 'marketing_engagement')}
                  <span class="over-text {{item.entity.marketing_engagement | lowercase}}">
                  {{item.entity.marketing_engagement}}</span>
                </div>
                <div class="${dual}">
                  <label>Web engagement:</label>
                  ${parseTemplate(columnDefs, 'website_engagement')}
                  <span class="over-text {{item.entity.website_engagement | lowercase}}">
                    {{item.entity.website_engagement}}</span>
                </div>
                <div class="${dual}">
                  <label>Trial Status:</label>
                  ${parseTemplate(columnDefs, 'trial_status')}
                  <span class="over-text {{item.entity.trial_status | lowercase}}">
                    {{item.entity.trial_status}}</span>
                </div>
                <div class="${dual}">
                  <label>Account Status:</label>
                  ${parseTemplate(columnDefs, 'account_status')}
                </div>
                <div class="${dual}">
                  <label>Expl. Close:</label>
                  ${parseTemplate(columnDefs, 'fiscal_period')}
                </div>
              </div>`;

  return template;
}

var template = (id, columnDefs, type) => {
  var tmp = "";
  var content = "";
  var size = classes;

  switch(type) {
    case 'list':
      content = listTemplate(columnDefs);
      size = fourths2;
    break;

    case 'tech-competency':
      content = defaultTemplate(columnDefs);
      size = dual;
    break;

    case 'engage':
      content = engageTemplate(columnDefs);
      size = thirds3;
    break;

    default:
      content = defaultTemplate(columnDefs);
    break;
  }

  tmp = `<div class="card-container" id="${id}">
          <div id="c-${id}" class="grid-card-container" ng-hide="!cardView">
            <div ng-repeat="item in data">
              <div class="grid-card-space ${size}" ng-if="!item.entity.placeholder">
                <div class="grid-card row ${type}">
                  ${content}
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="cards-placeholder ${fullCol}" ng-if="!!item.entity.placeholder">
                {{item.entity.placeholder}}
              </div>
            </div>
          </div>
        </div>`;

  return tmp;
}

function parseTemplate(columnDefs, field) {
  let column = _.find(columnDefs, {field: field});
  let template = `
    <span class="ui-grid-cell-contents">
      {{item.entity.${field}}}
    </span>`;

  if(column.cellTemplate) {
    template = column.cellTemplate;
    template = template.replace(/row.entity./g, 'item.entity.');
  }

  return template;
}

function getTitle(columnDefs, field) {
  let column = _.find(columnDefs, {field: field});
  return column.headerTooltip || column.displayName;
}

function displayName(columnDefs, field) {
  let column = _.find(columnDefs, {field: field});
  return column.displayName;
}

module.exports = template;
