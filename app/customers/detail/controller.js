'use strict';

var $ = require('jquery');
var paginationPageSizes = require('../../values.js').paginationSizes;

/**
 * @ngInject
 */
module.exports = class DetailController {
	constructor($rootScope, $state, $scope, $location, Restangular, promiseTracker,
		jqSparklineOptions, contextService, $stateParams) {
		'ngInject';

		this.$scope = $scope;
		this.contextService = contextService;
		this.promiseTracker = promiseTracker;
		this.accountId = $stateParams.accountId;
		this.state = $state;

		$scope.jqSparklineOptions = jqSparklineOptions;
		$scope.currentState = $state.current.name;

		this.sidebarOptions = {
			backLink: { name: 'Account Selection', state: 'customers' },
			sections: [
				{ name: 'Account Plan', state: 'customers.detail.overview' },
				{ name: 'Magellan', state: 'customers.detail.magellan' },
				{ name: 'Expansion Score', state: 'customers.detail.expansion-rating' },
				{ name: 'Technical Competency', state: 'customers.detail.technical-competency' }
			]
		}
		 
		this.gridStatsOptions = {
			data: [],
			paginationPageSize: 50,
			paginationPageSizes: paginationPageSizes,
			enablePaginationControls: true,
			enableFiltering: true,
			enableColumnMenus: false,
			exporterCsvFilename: 'myFile.csv',
			enableGridMenu: true,
			isCollapsed: false,
			columnDefs: [
				{
					displayName: 'Type',
					field: 'type',
					width: '30%'
				}, {
					displayName: 'Item',
					field: 'title',
					width: '70%',
					cellTemplate: `<span ng-if="row.entity.href" class="ui-grid-cell-contents">
														<a target="{{row.entity.target}}" href="{{row.entity.href}}">{{row.entity.title}}</a>
													</span>
													<span ng-if="!row.entity.href" class="ui-grid-cell-contents">{{row.entity.title}}</span>`
				}
			]
		};
		$scope.gridStatsOptions = this.gridStatsOptions;
	}

	$onInit() {
		var account = this.getAccount(this.accountId);
		this.$scope.customerTracker = this.promiseTracker();
		this.$scope.customerTracker.addPromise(account);
	}

	getAccount(accountId) {
		return this.contextService.getAccount(accountId)
			.then(data => {
				this.$scope.account = data;
				this.getAccountInfo(data);
				this.getStats(data);
			});
	}

	getAccountInfo(data) {
		var valid_customer = false;
		try {
			if (data.account_id == this.accountId) {
				valid_customer = true;
			}
		} catch (err) {
			//Not Allowed
		}

		if (!valid_customer) {
			this.state.transitionTo('customers');
		}

		if (data.api_platform && data.api_platform.indexOf('loud') > 0)
			data.cloud = true;
		else
			data.cloud = false;

		data.customerLogoUrl =
			"http://customerlogos.images.www-static.apigee.s3-website-us-west-2.amazonaws.com/internaluse/" +
			this.accountId;
		data.customerLogoPath = 'internaluse/' + this.accountId;

		this.$scope.account = data;
	}

	getStats(account) {
		var stats = [];

		stats.push({
			type: 'SFDC',
			title: account.account_name,
			href: 'https://apigee.my.salesforce.com/' + account.id,
			target: '_SFDC'
		});

		stats.push({
			type: '360',
			title: account.account_name,
			href: 'https://360.apigee.com/internal/#/customers/' + account.id,
			target: '_360'
		});

		stats.push({
			type: 'Industry',
			title: account.industry
		});

		stats.push({
			type: 'Sales Rep',
			title: account.sales_rep_name,
			href: 'mailto:' + account.sales_rep_email,
			target: ''
		});

		stats.push({
			type: 'Sales Stage',
			title: account.sales_stage
		});

		_.each(account.product_plans, plan => {
			stats.push({
				type: 'Entitlement',
				title: plan
			});
		});

		this.gridStatsOptions.data = stats;
	}

};
