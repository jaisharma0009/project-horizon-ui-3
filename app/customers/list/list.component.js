var template = require('./list.html');
var ListController = require('./controller');

let ListComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller: ListController,
  controllerAs: 'list'
};

module.exports = ListComponent;