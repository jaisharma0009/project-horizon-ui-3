var template = require('./template.html');
var ModelsController = require('./controller');

let Component = {
  restrict: 'E',
  bindings: {},
  template,
  controller: ModelsController,
  controllerAs: 'vm'
};

module.exports = Component;