'use strict';

module.exports = class EngageDetailsController {
  constructor($scope, $stateParams, HorizonService) {
    'ngInject';

    this.accountId = $stateParams.accountId;
    this.HorizonService = HorizonService;
    this.accountEngage = {};
  }

  $onInit() {
    let oppTracker = this.HorizonService
        .getEngageOpportunity(this.accountId)
        .then(opp => {
          if(ENV == 'local') {
            this.accountEngage = opp[0];
          } else {
            this.accountEngage = opp;
          }
        });
  }

}
