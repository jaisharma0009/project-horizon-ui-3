'use strict';

var angular = require('angular');
var angularUiRouter = require('@uirouter/angularjs').default;
var filters = require('../../filters');

module.exports = angular.module('app.engage.detail', [
    angularUiRouter,
    require('statearrow').name
  ])
  .config(function($stateProvider) {
     $stateProvider
      .state('engage.details', {
        url: '/{accountId}',
        component: 'engageDetails',
        data: {
          cssClasses: 'engage'
        }
      });
  })
  .component('engageDetails', require('./engage-details.component'))
