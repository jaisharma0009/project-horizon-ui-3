'use strict';

var angular = require('angular');
var angularUiRouter = require('@uirouter/angularjs').default;
var filters = require('../filters');

module.exports = angular.module('app.engage', [
    angularUiRouter,
    require('../services').name,
    require('./details').name,
    require('tsswitch').name
  ])
  .config(function($stateProvider) {
     $stateProvider
      .state('engage', {
        url: '/engage',
        component: 'engage',
        data: {
          cssClasses: 'narrow-body'
        }
      });
  })
  .component('engage', require('./engage.component'))
  .component('uTrialUsers', require('./uTrialUsers/component'))
  .component('uWebActivity', require('./uWebActivity/component'))
  .component('relevantInformation', require('./relevantInformation/component'))
  .component('marketingInformation', require('./marketingInformation/component'))
  .filter('stageFilter', filters.stageFilter)
  .filter('approximate', filters.approximate)
  .filter('noneDash', filters.noneDash)
  .filter('engageStatusClass', filters.engageStatusClass)
  .filter('engageStatusDotsClass', filters.engageStatusDotsClass)
  .filter('quarterFilter', filters.quarterFilter)
  .filter('myTextNullFilter', filters.myTextNullFilter)
  .filter('numberFilter', filters.myNumberFilter)
  .filter('hostname', filters.hostnameFilter)  
