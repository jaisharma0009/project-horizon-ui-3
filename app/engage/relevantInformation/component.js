var template = require('./template.html');
var RelevantInformationController = require('./controller');

let RelevantInformationComponent = {
  restrict: 'E',
  template,
  controller: RelevantInformationController,
  controllerAs: 'relevant',
  bindings: {
    account: '=',
    class: '@'
  }
};

module.exports = RelevantInformationComponent;