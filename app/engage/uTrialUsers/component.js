var template = require('./template.html');
var UTrialController = require('./controller');

let UTrialComponent = {
  restrict: 'E',
  template,
  controller: UTrialController,
  controllerAs: 'uTrialUsers'
};

module.exports = UTrialComponent;