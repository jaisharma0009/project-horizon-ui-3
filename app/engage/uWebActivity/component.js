var template = require('./template.html');
var UWebActivityController = require('./controller');

let UWebActivityComponent = {
  restrict: 'E',
  template,
  controller: UWebActivityController,
  controllerAs: 'uWebActivity'
};

module.exports = UWebActivityComponent;