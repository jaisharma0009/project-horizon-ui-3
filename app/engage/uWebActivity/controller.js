'use strict';

let paginationPageSizes = require('../../values.js').paginationSizes;
let customRow = require('../../values.js').customRow;
let placeholderRow = require('../../values.js').placeholderRow;
let customSort = require('../../values.js').customSort;

module.exports = class UWebActivityController {
  constructor($scope, $state, HorizonService) {
    'ngInject';

    this.$scope = $scope;
    this.HorizonService = HorizonService;
    this.customRow = customRow();
  }

  $onInit() {
    this.grid = {
      paginationPageSize: 100,
      paginationPageSizes: paginationPageSizes.sizes,
      enablePaginationControls: true,
      data: [],
      enableFiltering: true,
      exporterCsvFilename: 'myFile.csv',
      enableGridMenu: false,
      exporterMenuPdf: false,
      enableColumnMenus: false,
      rowHeight: 30,
      rowTemplate: this.customRow,
      columnDefs: [
        {
          displayName: 'Company',
          name: 'company',
          field: 'company',
          sortingAlgorithm: this.customSort
        },
        {
          displayName: 'Cumul. page views',
          name: 'views',
          field: 'views',
          sortingAlgorithm: this.customSort
        },
        {
          displayName: 'Cumul. time',
          name: 'time',
          field: 'time',
          sortingAlgorithm: this.customSort
        }
      ]
    }

    this.placeholderRow = placeholderRow(this.grid.columnDefs);
    this.getUnassociatedWebActivity();
  }

  getUnassociatedWebActivity() {
    this.HorizonService.getUnassociatedWebActivity()
      .then(activity => {
        activity.push(this.placeholderRow);
        this.grid.data = activity
      });
  }
}
