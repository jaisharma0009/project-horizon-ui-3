'use strict';

let d3 = require('d3');
let $ = require('jquery');
let _ = require('lodash');
let approx = require('approximate-number');

module.exports = class HBarChartController {
  constructor($scope, $element, $timeout) {
    'ngInject';

    this.$element = $element;
    this.$timeout = $timeout;
    this.$scope = $scope;
  }

  $onInit() {
    let resizeId;
    $(window).resize(() => {
      this.$timeout.cancel(resizeId);
      resizeId = this.$timeout(doneResizing.bind(this), 500);
    });
     
    function doneResizing(){
      this.$element.html("");

      this.$timeout(() => {
        this.initChart();
      }, 250);
    }

    this.$scope.$watch(() => this.data, newVal => {
      if (!!newVal) {
        this.$timeout(this.initChart.bind(this), 500);
      }
    })
  }

  initChart() {
    $(this.$element).html('');
    this.width = $(this.$element).parent().width() - 230;

    switch (this.chartSize) {
      case 'small':
        this.height = 190,
        this.barHeight = 143,
        this.barWidth = 26;
        this.paddingLeft = 120;
        this.axisSize = 20;
        break;

      default:
        this.height = 315,
        this.barHeight = 272,
        this.barWidth = 85;
        this.paddingLeft = 0;
        this.axisSize = 20;
        break;
    }

    let maxVal = this.maxValue(this.data).value;

    _.each(this.data, (item, index) => {

      let h = this.barWidth * 2 + this.axisSize;

      let svg = `<svg id="${this.chartId}-${index}" 
                          class="chart ${this.type} ${this.chartId}" 
                          height="${h}" 
                          width="${this.width}">
                     </svg>`;

      $(this.$element).append(svg);

      let name = index.split('_').join(' ');
      this.generateChart(item, `#${this.chartId}-${index}`, name, maxVal);
    });

    let charts = $(`.${this.chartId}`);
    $(charts).mouseenter(event => {
      $(charts).addClass('disabled');
      $(event.currentTarget).removeClass('disabled');
    });

    $(charts).mouseleave(event => {
      $(charts).removeClass('disabled');
    });
  }

  generateChart(data, selector, name, max) {
    let height = this.height,
      barHeight = this.barHeight,
      barWidth = this.barWidth,
      paddingLeft = this.paddingLeft;


    var y = d3.scale.linear()
      .domain([0, max])
      .range([0, this.width]);

    var chart = d3.select(selector)
      .attr("width", height);


    var bar = chart.selectAll("g")
      .data(data)
      .enter().append("g")
      .attr("transform", function (d, i) {
        let left = paddingLeft;
        let top = i * barWidth;

        return `translate(${left}, ${top})`;
      })
      .attr("height", barWidth)
      .attr("width", this.width)

    let rect = bar.append("rect")
      .attr("class", d => d.type)
      .attr("width", d => y(d.value))
      .attr("height", barWidth);
      
    rect.append("animate")
        .attr("attributeType", "XML")
        .attr("attributeName", "width")
        .attr("from", "0")
        .attr("to", d => y(d.value))
        .attr("dur", "0.5s")

    bar.append("text")
      .attr("class", d => {
        let hidden = "";

        if(this.axis) {
          //hidden = d.value*1 < 10000 ? "hidden" : '';
        }

        if(d.value*1 == 0) hidden = 'hidden';

        return `${d.type}-text ${hidden}`;
      })
      .attr("text-anchor", "middle")
      .attr("alignment-baseline", "middle")
      .attr("y", barWidth / 2)
      .attr("x", d => {
        /*if (this.type == 'blue-chart') {
          return  y(d.value) + 10;
        }

        let h = barHeight - (y(d.value));
        return barHeight - 10 - h;*/
        return 10;
      })
      .text(d => {
        return approx(d.value, { min10k: true });
      });

    let labelTopOffset = 12;
    if (this.type == 'blue-chart') {
      labelTopOffset = 0;
    }

    chart.append("text")
      .attr("class", "title-text")
      .attr("height", barWidth)
      .attr("text-anchor", "middle")
      .attr("y", barWidth)
      .attr("x", "35")
      .text(name);

      if(this.axis) {

        let xRange = d3.scale.linear().domain([0, max]).range([0, this.width]);

        let xAxis = d3.svg
        .axis()
        .scale(xRange)
        .orient('bottom')
        .ticks(6)
        .tickFormat((d, index) => approx(d, { min10k: true }));

        let left = paddingLeft;
        let top = 2 * barWidth;
        chart.append('g')
          .attr('class', 'axis')
          .attr('transform', `translate(${left}, ${top})`)
          .call(xAxis);
      }
  }

  capitalize(sstring) {
    return string[0].toUpperCase() + string.slice(1);
  }

  maxValue(data) {
    let max = [];

    _.each(data, item => {
      max.push(_.maxBy(item, o => o.value*1))
    })

    return _.max(max);
  }
}