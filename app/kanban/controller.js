'use strict';

var _ = require('lodash');
var $ = require('jquery');
var solve = require('logician');
var isNumber = require('is-number');

/**
 * @ngInject
 */
module.exports = class KanbanController {
	constructor ($scope, $state, $window, $timeout, Restangular, promiseTracker,
	jqSparklineOptions, kanbanService, $http, $rootScope) {
		'ngInject';

		this.scope = $scope;
		this.$timeout = $timeout;
		this.kanbanService = kanbanService;
		this.dragHistory = [];

		$scope.filters = {
			probabilities: ['20', '30', '40', '50', '60', '70', '80', '90'],
			opportunitytypes: [{id: 'all', 'displayName': 'All'}],
			selectedOpportunitytype: null,
			selectedProbability: '50',
			selectedAmount: 10000
		};

		$scope.filters.selectedOpportunitytype = $scope.filters.opportunitytypes[0];

		kanbanService.getOpportunityTypes()
			.then(function(types){
				_.each(types, function(type){
					$scope.filters.opportunitytypes.push({id: type.opportunity_type, displayName: type.opportunity_type});
				});
			});

		$scope.opportunities = {};
		$scope.quarters = [];
		$scope.opportunitiesTracker = promiseTracker();

		$scope.$watch('filters', this.getOpportunities.bind(this), true);
		//document.onkeydown = this.KeyUndoPress.bind(this);

		$scope.$on('EMPTY_DROP', this.emptyDrop);
		$scope.$on('REGULAR_DROP', this.regularDrop);
		$scope.$on('KEYS_UNDO', this.keyPress.bind(this));
	}

	emptyDrop(e, opp) {
		alert('Dropped to blank cell');
	}

	regularDrop(e, opp) {
		console.log('Regular drop');
	}

	getOpportunities(filters) {
		let opportunityType = false;

		if(filters.selectedOpportunitytype && filters.selectedOpportunitytype.id !== 'all'){
			opportunityType = filters.selectedOpportunitytype.id;
		}

		var opportunities = this.kanbanService.getOpportunitiesByQuarterList(filters.selectedAmount, filters.selectedProbability, false, opportunityType)
		.then(opp_types => {
			var quarters = [];
			_.each(opp_types, opp_type => {
				_.each(_.keys(opp_type), fq => {
					quarters.push(fq);
				});
			});
			quarters = _.uniq(quarters);
			quarters = _.sortBy(quarters);
			this.scope.quarters = quarters;
			this.scope.opportunities = opp_types;

			_.each(this.scope.opportunities, type => {
				_.each(quarters, q => {
					if(!type[q]) type[q] = new Array();
				});
			});

		});
		this.scope.opportunitiesTracker.addPromise(opportunities);
	}

	dndMoved(index, quarter, type, event) {
		let opps = this.scope.opportunities;
		let id = this.scope.opportunities[type][quarter][index].id;

		this.dragHistory.push({index, quarter, type, id});

		opps[type][quarter].splice(index, 1);

		let destOpp = this.findOpportunityById(id);
		let {selType, selQuarter, selIndex} = destOpp;

		if(!opps[selType][selQuarter][selIndex].original) {
			opps[selType][selQuarter][selIndex].original = {type, quarter, id};
		}

		let dropType = 'REGULAR_DROP';
		if(opps[selType][selQuarter].length === 1) {
			dropType = 'EMPTY_DROP';
		}

		this.scope.$emit(dropType, destOpp);
	}

	findOpportunityById(id) {
		let selType = null;
		let selIndex = null;
		let selQuarter = null;

		_.each(this.scope.opportunities, (quarter, tIndex) => {
			_.each(quarter, (q, qIndex) => {
				let index = _.findIndex(q, i => {
					return i.id == id
				});

				if(index != -1) {
					selType = tIndex;
					selQuarter = qIndex;
					selIndex = index;
				}
			});
		});

		if(selType == null) {
			return _.find(this.scope.opportunities, {id: id});
		}

		return {selType, selQuarter, selIndex};
	}

	restoreLastDrag() {
		let lastStep = this.dragHistory.pop();

		if(lastStep) {
			let opp = this.findOpportunityById(lastStep.id);
			let {selType, selQuarter, selIndex} = opp;
			let selected = this.scope.opportunities[selType][selQuarter][selIndex];

			this.scope.opportunities[selType][selQuarter].splice(selIndex,1);

			if(this.scope.opportunities[selType][selQuarter][selIndex]) {
				this.scope.opportunities[selType][selQuarter][selIndex].original = null;
			}

			let {type, quarter, index} = lastStep;
			this.scope.opportunities[type][quarter].splice(index, 0, selected);
		}
	}

	restoreDragAll() {
		while(this.dragHistory.length > 0) {
			this.restoreLastDrag();
		}
	}

	keyPress(e, evtobj) {
		this.restoreLastDrag();
		this.scope.$apply();
	}

	initQuarter(opps, quarter) {
		if(!opps[quarter]) {
			opps[quarter] = [];
		}
	}

};
