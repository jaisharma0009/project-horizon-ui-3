'use strict';

var angular = require('angular');

module.exports = angular.module('app.customers.services', [
    require('./userService').name,
    require('./kanbanService').name,
    require('./contextService').name,
    require('./presetsService').name,
    require('./horizonService').name
  ]);