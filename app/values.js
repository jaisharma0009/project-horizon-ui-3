let _ = require('lodash');

let paginationSizes = {
  default: 50,
  sizes: [
    { label: '10', value: 10 },
    { label: '25', value: 25 },
    { label: '50', value: 50 },
    { label: '100', value: 100 }
  ]
};

let paginationTemplate = `<div class="ui-grid-pager-panel" ui-grid-pager ng-show="grid.options.enablePaginationControls">
            <div class="ui-grid-pager-container" ng-if="paginationApi.getTotalPages() > 1">
              <div class="ui-grid-pager-control">
                <button type="button" ng-click="paginationApi.seek(1)" ng-disabled="cantPageBackward()">
                  <div class="first-triangle">
                    <div class="first-bar">
                    </div>
                  </div>
                  </button>
                <button type="button" ng-click="paginationApi.previousPage()" ng-disabled="cantPageBackward()">
                  <div class="first-triangle prev-triangle"></div></button> <input type="number" ng-model="grid.options.paginationCurrentPage"
                  min="1" max="{{ paginationApi.getTotalPages() }}" required> <span class="ui-grid-pager-max-pages-number" ng-show="paginationApi.getTotalPages() > 0">/ {{ paginationApi.getTotalPages() }}</span>
                <button type="button" ng-click="paginationApi.nextPage()" ng-disabled="cantPageForward()">
                  <div class="last-triangle next-triangle">
                  </div>
                  </button>
                <button type="button" ng-click="paginationApi.seek(paginationApi.getTotalPages())" ng-disabled="cantPageToLast()"><div class="last-triangle">
                  <div class="last-bar"></div></div></button></div>
              <div class="ui-grid-pager-row-count-picker">
                <select ng-model="grid.options.paginationPageSize">
                  <option ng-repeat="o in grid.options.paginationPageSizes track by $index" ng-value="o.value" value="{{o.value}}">
                    {{o.label}}
                  </option>
                </select>
                <span class="ui-grid-pager-row-count-label">
                    &nbsp;{{sizesLabel}}
                  </span>
              </div>
            </div>
            <div class="ui-grid-pager-count-container" ng-if="paginationApi.getTotalPages() > 1">
              <div class="ui-grid-pager-count"><span ng-show="grid.options.totalItems > 0">{{ 1 + paginationApi.getFirstRowIndex() }} <abbr ui-grid-one-bind-title="paginationThrough">-</abbr> {{ 1 + paginationApi.getLastRowIndex() }} {{paginationOf}} {{grid.options.totalItems}} {{totalItemsLabel}}</span></div>
            </div>
          </div>`;

let customRow =
  (sref = "") => `<div ng-if="!row.entity.placeholder" style="cursor:pointer" ${sref}
                    ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.uid" class="ui-grid-cell" 
                    ng-class="col.colIndex()" ui-grid-cell></div>
                <div ng-if="!!row.entity.placeholder" class="placeholder-row">
                  {{row.entity.placeholder}}
                </div>`;

let placeholderRow = (defs, placeholder = "No more information to show") => {
  let row = {};
  _.each(defs, item => {
    row[item.field] = '*';
  });
  row['placeholder'] = placeholder;
  return row;
}

let customSort = (a, b, rowA, rowB, direction) => {
  
  if (a == '*' && direction == 'asc') return 1;
  if (b == '*' && direction == 'asc') return -1;

  if (a == '*' && direction == 'desc') return -1;
  if (b == '*' && direction == 'desc') return -1;

  if (undefined === a && undefined === b) return 0;
  else if (undefined === a) return -1;
  else if (undefined === b) return 1;
  else {
    if (a < b) return -1;
    else if (a > b) return 1;
    else return 0;
  }
}

module.exports = {
  paginationSizes,
  paginationTemplate,
  customRow,
  placeholderRow,
  customSort
}