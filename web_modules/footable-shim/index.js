'use strict';

module.exports = window.footable || (function() {
  window.footable = require('footable');

  require('footable/css/footable.core.css');

  //require('footable/footable.filter');
  require('./footable.filter');
  require('footable/footable.sort');
  require('./footable.paginate');
  //require('footable/footable.memory');

  return window.footable;
})();
