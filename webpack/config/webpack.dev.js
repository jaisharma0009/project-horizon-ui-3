import webpack       from 'webpack';
import merge         from 'webpack-merge';
import webpackCommon from './webpack.common';
import config        from './config';

const componentHotLoader  = require.resolve('../loaders/component.loader');
const htmlLoader          = require.resolve('../loaders/html.loader');

export default function (options) {
  let commonConfig = webpackCommon(options);

  return merge(commonConfig, {
    devtool: '#cheap-module-eval-source-map',

    module: {
      rules: [
        {
          test: /\.js$/,
          enforce: 'pre',
          use: [
            {
              loader: componentHotLoader,
            }
          ],
          include: [ config.paths.app ],
          exclude: [ /node_modules/ ]
        },
        {
          test: /\.html$/,
          enforce: 'post',
          use: [
            {
              loader: htmlLoader,
            }
          ],
          include: [ config.paths.app ],
          exclude: [ /node_modules/, /app\/index\.template\.html$/ ]
        }
      ]
    },
    plugins: [

      new webpack.HotModuleReplacementPlugin(),

      new webpack.NoEmitOnErrorsPlugin(),
    ]
  });
};
